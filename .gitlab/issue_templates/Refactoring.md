### Summary

<!-- Short description piece of code to refactor. -->

### Improvment

<!-- What improvements give that refactor? -->

### Risk

<!-- Which components may be exposed to failure? -->

/label ~refactoring

