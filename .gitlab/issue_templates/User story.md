### Summary

As <who?> (user/admin)
will <what?> (function of product/error, that we want to solve)
to <why?> (why someone needs this functionality, what are the measurable benefits of introducing it).

### Acceptance Criteria

- [ ] Only the user should have access to the function
- [ ] The solution corresponds to the one contained in the attached model

### Test scenarios

Assuming… <describe of condition> – set initial conditions,
and… <another condition> – continuation of previous step,
when… <event> – event happen,
then… <result> – expected result.

### Additional information

(Additional information, models, files, links)

/label ~"user story"
