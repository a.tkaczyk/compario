<!--
IMPORTANT!!!

Before you will add new ticket with bug, check if this bug already exists.

Zanim utworzysz nowy ticket z błędem proszę przeszukaj listę ticketów filtrując po etykiecie "bug" i upewnij się,
że ticket który masz zamiar utworzyć nie jest duplikatem.

https://gitlab.com/a.tkaczyk/compario/-/issues?label_name%5B%5D=bug
-->

### Summary

<!-- quick summary of bug  -->

### Steps to repeat bug

<!-- Steps to repeat bug -->

### Current behavior

<!-- What is happening now-->

### Expected behavior

<!-- What should be happening now  -->

### Logs, screenshots, video etc.

<!-- Paste it here, remember about (```), for console output / input  -->

### Possible fixes

<!-- Paste here possible solution or helpful link / advice -->

/label ~bug
