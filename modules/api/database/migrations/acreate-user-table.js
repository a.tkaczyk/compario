const ranks = ['NORMAL', 'ADMIN']

module.exports = {
  up: async knex => {
    await knex.schema.createTable('users', table => {
      table.bigIncrements('id').primary()
      table.string('name').notNullable()
      table
        .string('email')
        .unique()
        .notNullable()
      table.string('password').notNullable()
      table
        .enum('rank', ranks)
        .notNullable()
        .default('NORMAL')
      table.timestamp('created_at', { useTz: false }).defaultTo(knex.fn.now())
      table.timestamp('updated_at', { useTz: false }).defaultTo(knex.fn.now())
    })
  },
  down: knex => {
    return knex.schema.dropTableIfExists('users')
  }
}
