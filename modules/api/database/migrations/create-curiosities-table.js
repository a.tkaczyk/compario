const types = ['CUSTOM', 'CURRENCY', 'WEIGHT', 'SPEED']
const statuses = ['CONFIRMED', 'PENDING', 'TO_DELETE']
exports.up = async knex => {
  await knex.schema.createTable('curiosities', table => {
    table.bigIncrements('id').primary()
    table
      .string('title')
      .notNullable()
      .unique()
    table.string('description')
    table.string('source')
    table
      .enum('type', types)
      .notNullable()
      .default('CUSTOM')
    table.decimal('value', 18, 9).unsigned()
    table.integer('rating').default(0)
    table
      .enum('status', statuses)
      .notNullable()
      .default('CUSTOM')
    table.timestamp('created_at', { useTz: false }).defaultTo(knex.fn.now())
    table.timestamp('updated_at', { useTz: false }).defaultTo(knex.fn.now())
    table.index(['value'])
    table.bigInteger('author_id').unsigned()
    table
      .foreign('author_id')
      .references('users.id')
      .onDelete('SET NULL')
  })
}

exports.down = async knex => {
  await knex.schema.dropTableIfExists('values')
}
