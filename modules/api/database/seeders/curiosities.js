exports.seed = async knex => {
  //  To cos sie będzie jebało, bo tu powinno wchodzić po obiekcie, a nie tak ja kteraz
  await knex('curiosities').insert([
    {
      title: 'Female African forest elephant',
      description:
        'It is the smallest of the three living elephant species, reaching a shoulder height of 2.4 m (7 ft 10 in)',
      source: 'https://en.wikipedia.org/wiki/African_forest_elephant',
      type: 'WEIGHT',
      value: 2700,
      status: 'CONFIRMED'
    },
    {
      title: 'Weight of tongue of blue whale',
      description:
        'Its tongue weighs around 2.7 tons. Its mouth is large enough to hold up to 90 tonnes of food and water.',
      source: 'https://www.scifacts.net/animals/blue-whale-tongue-size/',
      type: 'WEIGHT',
      value: 2700,
      rating: 12,
      status: 'CONFIRMED'
    }
  ])
}
