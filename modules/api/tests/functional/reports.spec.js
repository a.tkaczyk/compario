const { functional } = require('../setup')

const { Tester } = functional()

describe('GET /reports', () => {
  test('should return empty result when there is no data', async () => {
    const { statusCode, payload } = await Tester.request('GET', '/reports')

    expect(statusCode).toEqual(200)
    expect(payload).toMatchObject({
      data: []
    })
  })

  test('should return correct data when data is available', async () => {
    const {
      expenses: [{ date, value }]
    } = await Tester.hasImportedExpenses()

    const { statusCode, payload } = await Tester.request('GET', '/reports')

    expect(statusCode).toEqual(200)
    expect(payload).toMatchObject({
      data: [{ x: date.toISOString().substring(0, 10), y: value }]
    })
  })

  test('should return multiple series', async () => {
    const { expenses } = await Tester.hasImportedExpenses()
    const tag = await Tester.hasTag('Dom')

    await Tester.hasTaggedExpense(expenses[0].id, [tag.id])

    const { statusCode, payload } = await Tester.request('GET', '/reports')

    expect(statusCode).toEqual(200)
    expect(payload).toMatchObject({
      series: [
        {
          label: 'total',
          data: [
            {
              x: expenses[0].date.toISOString().substring(0, 10),
              y: expenses[0].value
            }
          ]
        },
        {
          label: 'Dom',
          data: [{ x: expenses[0].date.toISOString().substring(0, 10), y: expenses[0].value }]
        }
      ]
    })
  })
})

// const data = [{ x: '2019-01-01', y: 150.99 }, { x: '2019-01-02', y: 200 }]
