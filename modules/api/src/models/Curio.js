const Base = require('./Base')

class Curio extends Base {
  static get tableName() {
    return 'curiosities'
  }

  static get relationMappings() {
    const User = require('./User')
    return {
      recipes: {
        relation: Base.HasManyRelation,
        modelClass: User,
        join: {
          from: 'user.id',
          to: 'curiosities.author_id'
        }
      }
    }
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['title', 'source', 'type'],

      properties: {
        id: { type: 'string' },
        title: { type: 'string' },
        description: { type: 'string' },
        source: { type: 'string' },
        type: { type: 'string' },
        value: { type: 'number' },
        rating: { type: 'integer' },
        createdAt: { type: 'string', format: 'date' },
        updatedAt: { type: 'string', format: 'date' },
        authorId: { type: 'string' }
      }
    }
  }
}

module.exports = Curio
