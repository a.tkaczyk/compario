const { Model } = require('objection')
const { isNil } = require('ramda')
class Base extends Model {
  $beforeInsert() {
    if (this.constructor.timestamps) {
      const timestamp = new Date().toISOString()
      this.createdAt = timestamp
      this.updatedAt = timestamp
    }
  }

  $beforeUpdate() {
    if (this.constructor.timestamps) {
      this.updatedAt = new Date().toISOString()
    }
  }

  $parseDatabaseJson(json) {
    json = super.$parseDatabaseJson(json)

    Object.keys(json).forEach(prop => {
      const value = json[prop]

      if (value instanceof Date) {
        json[prop] = value.toISOString()
      }
    })

    return json
  }

  getTableName() {
    return this.constructor.tableName
  }

  static async exists(db, id) {
    return (
      (await this.query(db)
        .findById(id)
        .select(1)) !== undefined
    )
  }

  static get pickJsonSchemaProperties() {
    return true
  }

  static query(db) {
    const knex = (() => {
      if (isNil(db)) {
        return super.query(db)
      }

      if (typeof db.getKnex === 'function') {
        return super.query(db.getKnex())
      }

      return super.query(db)
    })()

    return Object.assign(Object.create(knex), {
      async exists(...whereArgs) {
        return !isNil(await this.findOne(...whereArgs))
      }
    })
  }
}

module.exports = Base
