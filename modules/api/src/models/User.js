const Base = require('./Base')

class User extends Base {
  static get tableName() {
    return 'Users'
  }

  static async getByEmail(db, email) {
    return this.query(db).findOne('email', email)
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name', 'email', 'password', 'rank'],

      properties: {
        id: { type: 'string' },
        name: { type: 'string' },
        email: { type: 'string' },
        password: { type: 'string' },
        rank: { type: 'string' },
        score: { type: 'integer' }
      }
    }
  }

  $formatJson() {
    const data = {
      id: this.id,
      name: this.name,
      email: this.email
    }

    return data
  }
}

module.exports = User
