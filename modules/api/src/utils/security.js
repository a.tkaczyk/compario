const { promisify } = require('util')
const crypto = require('crypto')
const randomBytes = promisify(crypto.randomBytes)
const securityConfig = require('../../config/config')().security
const bcrypt = require('bcrypt')
const JWT = require('jsonwebtoken')

module.exports = {
  randomToken: async (length) => (await randomBytes(length / 2)).toString('hex'),

  hashPassword: async (password) => bcrypt.hash(password, securityConfig.bcrypt.saltRounds),

  verifyPassword: async (password, expectedHash) => bcrypt.compare(password, expectedHash),

  createToken: (user) =>
    JWT.sign(
      {
        id: user.id,
        email: user.email
      },
      securityConfig.jwt.privateKey,
      { algorithm: securityConfig.jwt.algorithm, expiresIn: 181 * 60 }
    )
}
