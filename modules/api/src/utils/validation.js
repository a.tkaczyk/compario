const Joi = require('joi')
const { isNil } = require('ramda')
const { ValidationError } = require('./common')
const xss = require('xss')

const validate = (schema, data) => {
  const { error, value } = Joi.validate(data, schema, { abortEarly: false })
  if (error) {
    const errors = {}
    error.details.forEach(element => {
      const key = element.context.key || element.context.label
      if (!isNil(errors[key])) {
        errors[key].push(element.message)
      } else {
        errors[key] = [element.message]
      }
    })

    throw new ValidationError(errors)
  }
  return value
}

const ExtendedJoi = Joi.extend(joi => ({
  base: joi.string(),
  name: 'string',
  language: {
    contains_html: 'not allowed to contain html'
  },
  pre(value, state, options) {
    if (this._flags.allowHtml !== true && /[<>]/.test(value)) {
      try {
        xss(value, {
          onTag(tag, html, options) {
            throw new Error()
          }
        })
      } catch (err) {
        return this.createError(
          'string.contains_html',
          { v: value },
          state,
          options
        )
      }
    }

    return value
  },
  rules: [
    {
      name: 'allowHtml',
      setup(arg1) {
        this._flags.allowHtml = true
      }
    }
  ]
}))

module.exports = {
  Joi: ExtendedJoi,
  ValidationError,
  validate
}
