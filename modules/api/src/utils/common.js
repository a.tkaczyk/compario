const Joi = require('joi')
const { isNil } = require('ramda')

exports.validate = (schema, data) => {
  const { error, value } = schema.validate(data, { abortEarly: false })
  if (error) {
    const errors = {}
    error.details.forEach(element => {
      const key = element.context.key || element.context.label
      if (!isNil(errors[key])) {
        errors[key].push(element.message)
      } else {
        errors[key] = [element.message]
      }
    })

    const err = new Error('Validation Error')
    err.errors = errors
    throw err
  }
  return value
}

exports.swaggerHeaders = (headers = {}) => ({
  headers: Joi.object({ ...headers }).unknown()
})

exports.swaggerResponses = (successCode = 200) => {
  const responses = {
    204: {
      description: 'No Content',
      schema: Joi.object({
        statusCode: Joi.number().default(204),
        message: Joi.string().default("Don't send any payload to your client")
      })
    },
    400: {
      description: 'Bad Request',
      schema: Joi.object({
        statusCode: Joi.number().default(400),
        message: Joi.string().default('Some logical error with the request')
      })
    },
    401: {
      description: 'Unauthorized',
      schema: Joi.object({
        statusCode: Joi.number().default(401),
        message: Joi.string().default('Missing or bad authentication')
      })
    },
    403: {
      description: 'Forbidden',
      schema: Joi.object({
        statusCode: Joi.number().default(403),
        message: Joi.string().default(
          'Not authorized to perform the requested operation'
        )
      })
    },
    422: {
      description: 'Unprocessable Entity',
      schema: Joi.object({
        statusCode: Joi.number().default(422),
        message: Joi.string().default('Payload validation has failed'),
        errors: Joi.object({
          email: Joi.array().items('email must be a valid email'),
          password: Joi.array().items('password is required')
        })
      })
    },
    500: {
      description: 'Internal Server Error',
      schema: Joi.object({
        statusCode: Joi.number().default(500),
        message: Joi.string().default('Something went wrong on our side')
      })
    }
  }
  responses[successCode] = {}
  return { responses }
}
