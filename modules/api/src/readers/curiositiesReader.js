const Curiosities = require('../models/Curio')

exports.getTags = async (db, { query, page = 0, perPage = 10 }) => {
  const qb = Curiosities.query(db)

  if (query) {
    qb.where('name', 'like', `%${query}%`)
  }

  return qb.orderBy('id', 'ASC').page(page, perPage)
}
