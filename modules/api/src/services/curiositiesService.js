const Curio = require('../models/Curio')
const Boom = require('@hapi/boom')

exports.createCurio = async (db, name) => {
  const isDuplicated = await Curio.query(db)
    .where('name', name)
    .first()
  if (isDuplicated) {
    throw Boom.badData(null, { name: ['"name" should be unique'] })
  }

  return Curio.query(db).insert({ name })
}
