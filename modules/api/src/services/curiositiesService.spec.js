const fs = require('fs')
const path = require('path')

const { Tester, dbTransactions, db, S3, initS3TestBucket } = require('../utils/tests').integration()
const service = require('./recipeService')
const Recipe = require('../models/Recipe')
const UserFoodEvent = require('../models/UserFoodEvent')
const { wrapReadableStreamInUploadFile } = require('../utils/data/uploadFile')

initS3TestBucket()

dbTransactions()

let admin

beforeEach(async () => {
  admin = await Tester.haveAdmin()
  // user = await Tester.haveUser()
})

describe('createCurio', () => {
  test('can create a new curio', async () => {
    Tester.haveCurio()
    const author = await Tester.haveUser()

    const res = await service.createRecipe(db, S3, admin, {
      name: 'Scrambled eggs',
      description: 'Some description',
      preparationTime: 10,
      ingredients: [{ id: foodItem1.id, amount: 233 }, { id: foodItem2.id, amount: 219 }],
      authorId: author.id,
      recipePhotoFile: wrapReadableStreamInUploadFile(
        fs.createReadStream(`${__dirname}/../../tests/fixtures/photos/derivatives/person.jpg`)
      )
    })

    expect(res).toMatchObject({
      name: 'Scrambled eggs',
      description: 'Some description',
      preparationTime: 10,
      ingredients: [{ id: foodItem1.id, amount: 233 }, { id: foodItem2.id, amount: 219 }],
      authorId: author.id
    })

    expect(await Tester.checkKeyExistsInPrivateS3(res.photoPath)).toBeTruthy()
    const photoPathStruct = path.parse(res.photoPath)
    expect(
      await Tester.checkKeyExistsInPublicS3(
        `${photoPathStruct.dir}/${photoPathStruct.name}-500-500${photoPathStruct.ext}`
      )
    ).toBeTruthy()
    expect(
      await Tester.checkKeyExistsInPublicS3(
        `${photoPathStruct.dir}/${photoPathStruct.name}-140-140${photoPathStruct.ext}`
      )
    ).toBeTruthy()
  })

  test('validates required basic paratemers', async () => {
    try {
      await service.createRecipe(db, S3, admin, {})
      throw new Error('Exception should be thrown')
    } catch (err) {
      expect(err).toBeDefined()
      expect(err.message).toEqual('Validation Error')
      expect(err.invalidArgs).toMatchSnapshot()
    }
  })
})