const Joi = require('@hapi/joi')
const { createCurio } = require('../src/services/curiositiesService')
const { getCuriosities } = require('../src/readers/curiositiesReader')

module.exports = [
  {
    method: 'GET',
    path: '/curiosities',
    config: {
      auth: false,
      description: 'Get all curiosities',
      notes: 'Get curiosities',
      tags: ['api'],
      validate: {
        query: Joi.object({
          page: Joi.number()
            .integer()
            .optional(),
          perPage: Joi.number()
            .integer()
            .optional(),
          query: Joi.string().optional()
        })
      }
    },
    handler: async ({ db, query }, h) => {
      return getCuriosities(db, query)
    }
  },
  {
    method: 'POST',
    path: '/curiosities',
    config: {
      auth: false,
      description: 'Post curiosities',
      notes: 'Post curiosities',
      tags: ['api'],
      validate: {
        payload: Joi.object({
          name: Joi.string().required()
        })
      }
    },
    handler: async ({ db, payload: { name } }, h) => {
      const tag = await createCurio(db, name)
      return h.response(tag).code(201)
    }
  }
  // {
  //   method: 'DELETE',
  //   path: '/tags/{tagId}',
  //   config: {
  //     auth: false,
  //     description: 'Delete tags',
  //     notes: 'Delete tags',
  //     tags: ['api'],
  //     validate: {
  //       params: Joi.object({
  //         tagId: Joi.number().integer()
  //       })
  //     }
  //   },
  //   handler: async ({ db, params: { tagId } }, h) => {
  //     await deleteTag(db, tagId)
  //     return h.response().code(204)
  //   }
  // },
  // {
  //   method: 'PATCH',
  //   path: '/tags/{tagId}',
  //   config: {
  //     auth: false,
  //     description: 'Change tag name by its id',
  //     notes: 'Change tag name',
  //     tags: ['api'],
  //     validate: {
  //       params: Joi.object({
  //         tagId: Joi.number().integer()
  //       }),
  //       payload: Joi.object({
  //         name: Joi.string().required()
  //       })
  //     }
  //   },
  //   handler: async ({ db, params: { tagId }, payload: { name } }, h) => {
  //     return updateTag(db, tagId, name)
  //   }
  // }
]
