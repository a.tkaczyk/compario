const controller = require('../src/controllers/expenseController')

module.exports = [
  {
    method: 'GET',
    path: '/expenses',
    config: controller.index
  },
  {
    method: 'GET',
    path: '/expenses/{expenseId}',
    config: controller.show
  },
  {
    method: 'DELETE',
    path: '/expenses/{expenseId}',
    config: controller.destory
  },
  {
    method: 'PATCH',
    path: '/expenses/{expenseId}',
    config: controller.update
  },
  {
    method: 'PUT',
    path: '/expenses/{expenseId}/tags',
    config: controller.syncTags
  }
]
