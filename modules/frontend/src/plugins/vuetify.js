import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import en from 'vuetify/es5/locale/en'
import pl from 'vuetify/es5/locale/pl'

Vue.use(Vuetify, {
  lang: {
    locales: { en, pl },
    current: document.documentElement.lang
  }
})

export default new Vuetify({})
