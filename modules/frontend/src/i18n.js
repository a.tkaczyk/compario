import Vue from 'vue'
import VueI18n from 'vue-i18n'
import messages from './lang/locales'

const defaultGetChoiceIndex = VueI18n.prototype.getChoiceIndex

VueI18n.prototype.getChoiceIndex = function(choice, choicesLength) {
  if (this.locale !== 'pl') {
    return defaultGetChoiceIndex.bind(this)(choice, choicesLength)
  }

  if (choice === 0) {
    return 0
  }

  if (choice === 1) {
    return 1
  }

  if (choice > 1 && choice < 5) {
    return 2
  }

  return 3
}

Vue.use(VueI18n)

const i18n = new VueI18n({
  messages,
  locale: document.documentElement.lang || 'en',
  fallbackLocale: 'en'
})

export default i18n
