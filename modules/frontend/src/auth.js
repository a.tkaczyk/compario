import '@mdi/font/css/materialdesignicons.css'

import Vue from 'vue'
import i18n from './i18n'
import Router from 'vue-router'
import Vuetify from 'vuetify'
import 'vuetify/src/stylus/app.styl'
import colors from 'vuetify/es5/util/colors'
import en from 'vuetify/es5/locale/en'
import pl from 'vuetify/es5/locale/pl'

Vue.use(Vuetify, {
  iconfont: 'mdi',
  theme: {
    primary: colors.deepPurple.base,
    secondary: colors.deepOrange.base,
    accent: '#64cf71',
    error: colors.red.accent2,
    info: colors.cyan.base,
    success: colors.green.base,
    warning: colors.amber.base
  },
  lang: {
    locales: { en, pl },
    current: document.documentElement.lang
  }
})

Vue.use(Router)

;(async () =>
  new Vue({
    i18n,
    el: '#app'
  }))()
