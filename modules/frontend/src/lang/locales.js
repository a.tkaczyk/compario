export default {
  en: {
    speechRecognition: {
      error:
        'An unexpected error occurred with the speech recognition function in this browser'
    },
    errors: {
      given_name_already_taken: 'The given name is already taken'
    },
    common: {
      help: 'Help',
      next: 'Next',
      yes: 'Yes',
      no: 'No'
    },

    auth: {
      login_using_facebook: 'Sign in with Facebook'
    },

    passwordReset: {
      reset_password: 'Reset password'
    },

    userAccount: {
      account: 'Account'
    },

    foodItems: {
      original_name: 'Original name',
      food_items: 'Food items',
      add_as_new_food_item: 'Add as a new food item',
      import_update: 'Import this update',
      ignore_food_item: 'Ignore food item',
      replace_selected_food_item: 'Replace selected food item',
      existing_food_items: 'Existing food items',
      pending_food_items: 'Pending food items',
      you_are_importing: 'You are importing {item}:',
      you_are_updating: 'You are updating {item}:',
      comparison_of_nutritional_values: 'Comparison of nutritional values',
      name: 'Description',
      amount: 'Quantity',
      grams_per_item: 'Grams per item',
      unit_suffix_items: 'items',
      hint_of_grams_per_item: 'How much weights a single item?',
      count_of_items: 'zero items|one item|{n} items',
      add_product: 'Add product',
      nutrients: 'Nutrients',
      current_value: 'Current value',
      latest_value: 'Latest value',
      value_in_100_g_of_the_product: 'Value in 100 g of the product',
      give_the_product_name: 'Give the product name',
      click_to_add_value: 'Click to add value',
      minerals: 'Minerals',
      vitamins: 'Vitamin',
      amino_acids: 'Amino acids',
      seasonality: 'Seasonality',
      enter_the_quantity: 'Enter the quantity',
      enter_keywords: 'Enter keywords',
      carbohydrate: 'Carbohydrates (g)',
      energy: 'Energy (kcal)',
      protein: 'Protein (g)',
      fat: 'Fat (g)',
      sugar: 'Sugar (g)',
      fiber: 'Fiber (g)',
      fatty_acids_saturated: 'Fatty acids saturated (g)',
      fatty_acids_monounsaturated: 'Fatty acids monounsaturated (g)',
      fatty_acids_polyunsaturated: 'Fatty acids polyunsaturated (g)',
      fatty_acids_trans: 'Fatty acids trans (g)',
      fatty_acids_omega3: 'Fatty acids omega 3 (g)',
      fatty_acids_omega6: 'Fatty acids omega 6 (g)',
      sodium: 'Sodium (mg)',
      potassium: 'Potassium (mg)',
      calcium: 'Calcium (mg)',
      magnesium: 'Magnesium (mg)',
      iron: 'Iron (mg)',
      zinc: 'Zinc (mg)',
      copper: 'Copper (mg)',
      manganese: 'Manganese (mg)',
      vitamin_a: 'Witamin A (µg)',
      vitamin_e: 'Witamin E (µg)',
      vitamin_d: 'Witamin D (µg)',
      vitamin_c: 'Witamin C (mg)',
      vitamin_b6: 'Witamin B6 (mg)',
      vitamin_b12: 'Witamin B12 (mg)',
      processed: 'Processed',
      not_processed: 'Not processed',
      ean_code: 'EAN code',
      ean_codes: 'EAN codes',
      add_ean_code: 'Add EAN code'
    },

    onboarding: {
      plan: 'Plan'
    },

    admin: {
      admin_panel: 'Admin panel'
    },

    validation: {
      email: 'Must be valid e-mail',
      required: '{field} is required',
      integer: '{field} must be an integer',
      numeric: '{field} must be a number',
      decimal: '{field} must be a decimal number',
      listNotEmpty: 'At least one ingredient must be added to {field}',
      between: '{field} should be between {min} and {max}',
      minValue: '{field} should be no less than {min}',
      sameAs: 'These values must be the same',
      minLength: 'Field {field} must have at least {min} characters',

      fields: {
        password: 'password',
        passwordConfirmation: 'password confirmation',
        id: 'id',
        email: 'email',
        firstName: 'first name',
        lastName: 'last name',
        name: 'name'
      }
    }
  },

  pl: {
    speechRecognition: {
      error:
        'Wystąpił nieoczekiwany błąd z funkcją rozpoznawania mowy w tej przeglądarce.',
      say_something_to_search: 'Powiedz coś, aby wyszukać.'
    },

    errors: {
      given_name_already_taken: 'Podana nazwa jest już zajęta'
    },

    common: {
      help: 'Pomoc',
      next: 'Dalej',
      yes: 'Tak',
      no: 'Nie'
    }
  }
}
