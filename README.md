Compario

### Opis

Concept of Compario is simple - Comparator of many values. We enter a value into the search bar, choose unit (like currency, weight, speed etc.) and the application shows approximate 

more on https://tkaczyk.me/compario.html

### Instrukcja

```
./dev start
./dev stop
```

http://localhost:3000

helpful commands

```
./dev logs
./dev logs <app/db>
./dev reset
./dev restart
./dev ps
./dev yarn knex
./dev yarn test
```
